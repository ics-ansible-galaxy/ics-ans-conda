# ics-ans-conda

This Ansible playbook is for dev/test purpose and allows to install conda on a server.
It uses the "all" group. Make sure to use a limit when running it!

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## License

BSD 2-clause
