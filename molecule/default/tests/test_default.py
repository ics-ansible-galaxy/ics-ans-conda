import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_conda_installed(host):
    cmd = host.run('/opt/conda/bin/conda --help')
    assert cmd.rc == 0
